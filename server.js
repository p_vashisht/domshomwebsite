// server.js

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();

var passport = require('passport');
//var flash    = require('connect-flash');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var cors         = require('cors');


let http = require('http').Server(app);
let io = require('socket.io')(http);

var log4js = require('log4js');
var log4js_extend = require("log4js-extend");

log4js.configure('log4js/config.json', { reloadSecs: 1000 });

log4js_extend(log4js, {
    path: __dirname,
    format: ":>> log at @name (@file-@line:@column)"
});

var logger = log4js.getLogger('MyLogger');
logger.setLevel('debug');

/*
logger.trace('Entering cheese testing in blue');
logger.debug('Got cheese in sky blue');
logger.info('Cheese is Gouda in green');
logger.warn('Cheese is quite smelly in yellow');
logger.error('Cheese is too ripe! in red');
logger.fatal('Cheese was breeding ground for listeria in purple');
*/


var mongoose = require('mongoose');


var configDB = require('./server/routes/config/database.js');

// configuration ===============================================================
var db = mongoose.connect(configDB.url); // connect to our database


// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

// app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
// app.use(session({ secret: '50m353(r3tkey' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
//app.use(flash()); // use connect-flash for flash messages stored in session
app.use(cors());
app.options('*', cors());
 
// Configuration from APP
//app.use(express.static('static'));
app.use('/', express.static(__dirname + '/client/dist'));
app.use('/slides/', express.static(__dirname + '/client-slides'));

/*app.get("/slides/:slide", function(req, res){
    console.log(req.params.slide);
    res.send(200);
});*/

// app.use('/slides/angular2', express.static(__dirname + '/angular2'));
// app.use('/slides/html', express.static(__dirname + '/html'));
// app.use('/slides/html5', express.static(__dirname + '/html5'));
// app.use('/slides/java8', express.static(__dirname + '/java8'));
// app.use('/slides/react', express.static(__dirname + '/react'));
// app.use('/slides/css3', express.static(__dirname + '/css3'));

var port     = process.env.NODE_PORT || 4000;
var host     = process.env.NODE_IP;



/*
let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);

io.on('connection', (socket) => {
  console.log('user connected');

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

  socket.on('add-message', (message) => {
    io.emit('message', {type:'new-message', text: message});
  });
});

http.listen(5000, () => {
  console.log('started on port 5000');
});
*/


// launch ======================================================================
/*
if(host) {
    app.listen(port, host, function () {
        logger.info("The magic happens on  " + host + ":" + port);
    });
} else {
    app.listen(port, function(){
        logger.info( "The magic happens on  localhost:" + port );
    });
}
*/
if(host) {
	http.listen(port, host, () => {
        logger.info("The magic happens on  " + host + ":" + port);
	});
} else {
	http.listen(port, () => {
        logger.info("The magic happens on:" + port);
	});
}


 
// {"json":{"host":"127.7.35.129"}}
app.post('/host',function (req,res) {
    var json = {host: false};

    if(host)
        json = {host: host};

    res.json({json:json});
});

//require('./app/routes.js')(app, passport, logger); // load our routes and pass in our app and fully configured passport
require('./server/routes.js')(app, passport, logger); // load our routes and pass in our app and fully configured passport
require('./server/sockets.js')(io, logger);

// video scoket
require('./server/videoSocket.js')(io, logger);
 