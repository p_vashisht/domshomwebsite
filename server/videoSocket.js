/*var log4js = require('log4js');
log4js.configure('log4js/config.json', { reloadSecs: 1000 });

var logger = log4js.getLogger('VideoSocketLogger');
logger.setLevel('trace');
*/

var BinaryServer = require('binaryjs').BinaryServer;
var fs = require('fs');

var server = BinaryServer({port: 4400});

module.exports = function (io, logger) {
    server.on('connection', function(client){
        var videodir = __dirname+'/videos/videolib/';

        client.on('stream', function(stream, data){
            logger.debug(data);
            videodir = videodir + data.videoSubject + data.video;

            logger.debug(videodir);

            var file = fs.createReadStream(videodir);
            logger.info('file sent to client');
            client.send(file);
        });

    });

    server.on('close', function(){
        logger.info('client disconnected');
    });
    logger.info("magic happens at localhost:4400");
}