var models = require('./models/schema');
var fs = require('fs');
var authroutes = require('./routes/authroutes');
var quizroutes = require('./routes/quizroutes');
var pollroutes = require('./routes/pollroutes');
var contactroutes = require('./routes/contactroutes');

var userroutes = require('./routes/internalUse/user');

var setup = require('./routes/internalUse/setup');
var trainingSetup = require('./routes/internalUse/trainingSetup');

module.exports = function (app, passport, logger) {

    // pass passport for configuration
    require('./routes/config/passport')(passport, logger);

    // authruotes
    authroutes(app, passport, logger);

    // setup
    setup(app, logger);
    // quizroutes
    quizroutes(app, passport, logger);
    // suryverroutes
    pollroutes(app, logger);
    // contactroutes
    contactroutes(app, logger);
	
	
    userroutes(app, logger);
    // socketApp
//    socketapp(io, logger, app);

// trainingSetup
trainingSetup(app, logger);

/*
    app.get('/processes', function(req, res){
        if(process.env)
            res.json({success: process.env});
        else
            res.json({success: false});
    });
*/

};
