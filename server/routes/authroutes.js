
var models = require('./../models/schema');
var User = require('./../models/user');

//var isLoggedInForRest = require('./util/isLoggedInForRest');
var isLoggedIn = require('./util/isLoggedIn');


const getToken = require('./config/token');

// const passport = require('passport');


module.exports = function (app, passport, logger) {

    const requireSignin = passport.authenticate('local-login', { session: false });
    const requireSignup = passport.authenticate('signup', { session: false });
    const requireAuth = passport.authenticate('jwt', { session: false });


    var signin = function (req, res, next) {
        logger.debug('In Sign In *****');
        logger.debug(req.user);
        logger.debug("*******************************************************");
        res.send({
            user: {
                email: req.user.local.email,
                // isAuthenticated: true,
                token: getToken(req.user)
            }
        });
    }
    app.post('/login', requireSignin, signin);

    var signup = function (req, res, next) {
        const email = req.body.email;
        const password = req.body.password;

        if (!email || !password) {
            return res.status(422).json({ error: 'You must provide email and password' });
        }

        // See if a user with the given email exists
        User.findOne({ "local.email": email }, function (err, existingUser) {
            if (err) { return next(err); }

            // If a user with email does exist, return an error
            if (existingUser) {
                return res.status(422).json({ error: 'Email is in use' });
            }

            // If a user with email does NOT exist, create and save user record
            const user = new User({
                "local.email": email
                // "local.password": User.generateHash(password)
            });
            user.local.password = user.generateHash(password)

            user.save(function (err) {
                if (err) { return next(err); }

                // assign default quiz
            var userTraining = new models.UserTraining({
                user: user.local.email,
                status: "accepted",
                trainingId: 101
            })

            userTraining.save(function (err) {
                if (err) throw err;
                logger.info('UserTraining saved');
            })

/*		var trainingQuiz = new models.TrainingQuiz({
            trainingId: 101,
            quiz: 101,
            quizName: "CSS",
            description: "Default quiz"
        })
        trainingQuiz.save(function (err) {
            if (err) throw err;
            logger.info('TrainingQuiz saved');
        })
*/


                // Repond to request indicating the user was created
                res.json({
                    user: {
                        email: user.local.email,
                        // role: user.local.role,
                        // isAuthenticated: true,
                        token: getToken(user)
                    }
                });


            });
        });
    }

    app.post('/signup', signup);


    // send to facebook to do the authentication
    app.get('/auth/facebook', passport.authenticate('facebook', {
        scope: 'email'
    }));



    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));


    app.post('/feedback', requireAuth, function (req, res) {
        //app.post('/feedback',isLoggedInForRest, function (req, res) {

        // console.log(JSON.stringify(req.headers));
        // logger.debug(req.get("Authorization"))

        logger.debug("in feedback route");
        logger.debug(req.body);

        var feedback = new models.Feedback({
            email: req.body.email,
            feedback: req.body.feedback,
            rate: req.body.rate
        });
        logger.debug(feedback);

        feedback.save(function (err) {
            if (err) throw err;
            logger.debug('Feedback saved')
        });


        res.json({
            feedback: feedback
        });

    });


    app.get('/user/scores', function (req, res) {
        //    app.get('/user/scores', isLoggedInForRest, function (req, res) {
        //routes.post('/findQuizQuestions', function (req, res) {
        // logger.debug(req.get("Authorization"))

        //logger.debug(req);
        logger.debug(req.user.local.email);

        models.UserScore.find({
            username: req.user.local.email
        })
            //.populate('question')
            .exec(function (err, scores) {
                if (err) throw err;

                logger.debug('Printing Data ')
                logger.debug(scores);
                var responseJson = {
                    scoreAvailable: false
                };
                if (scores.length > 0) {
                    /*scores.forEach(function (userscore) {
                        logger.debug(userscore);
                    });*/
                    responseJson.scoreAvailable = true;
                    responseJson.scores = scores;
                }
                res.json(responseJson);
            });
    });



}