var models = require('./../../models/schema');
var fs = require('fs');

module.exports = function (app, logger) {

    app.post("/setupDefaultTraining", function(req, res){
        var training = new models.Training({
            _id: 101,
            name: "Default Training",
            uniqueName: "DefaultTraining_xxx",
            description: "All users will be part of this training."
        })
        training.save(function (err) {
            if (err) throw err;
            logger.info('Default training saved')
        })
        var trainingQuiz = new models.TrainingQuiz({
            trainingId: 101,
            quiz: 101,
            quizName: "CSS",
            description: "Default quiz"
        })
        trainingQuiz.save(function (err) {
            if (err) throw err;
            logger.info('TrainingQuiz saved');
        })


        res.json({
            success: true
        });
    })

    app.post("/setupTraining", function(req, res){
        var training1 = new models.Training({
            _id: 1,
            uniqueName: "FirstTraining_xxx",
            name: "First Training",
            description: "Demo training is here"
        })
        var training2 = new models.Training({
            _id: 2,
            uniqueName: "SecondTraining_xxx",
            name: "Second Training",
            description: "Demo training is again here"
        })
        training1.save(function (err) {
            if (err) throw err;
            logger.info('Training 1 saved')
        })

        training2.save(function (err) {
            if (err) throw err;
            logger.info('Training 2 saved')
        })
        res.json({
            success: true
        });
    })

    // user tkhts@gmail.com
    app.post('/setupUserTraining', function (req, res) {
        var userTraining1 = new models.UserTraining({
            user: 'tkhts@gmail.com',
            // quiz: 9,
            // quizName: "HTML",
            // description: quiz1.description,
            trainingId: 1
        })
        var userTraining2 = new models.UserTraining({
            user: 'puneetvashisht@gmail.com',
            trainingId: 2
        })

        userTraining1.save(function (err) {
            if (err) throw err;
            logger.info('UserTraining1 saved');
        })
        userTraining2.save(function (err) {
            if (err) throw err;
            logger.info('UserTraining2 saved');
        })

        res.json({
            success: true
        });
    });



    app.post('/setupTrainingQuiz', function (req, res) {

//=========================================
/*
//++++++++++++++
        var quiz1 = new models.Quiz({
            _id: 9,
            name: 'HTML',
            description: 'HtmlQuiz',
            showScores: true
        });
        var quiz2 = new models.Quiz({
            _id: 10,
            name: 'Java',
            description: 'Java Quiz',
            showScores: false
        });

        quiz1.save(function (err) {
            if (err) throw err;
            logger.info('Html Quiz saved')
        })

        quiz2.save(function (err) {
            if (err) throw err;
            logger.info('Java Quiz saved')
        })
//++++++++++++++

        var answers11 = new models.Answer({
            text: 'correct answer',
            correct: true
        })
        var answers12 = new models.Answer({
            text: 'incorrect answer',
            correct: false
        })
        var answers13 = new models.Answer({
            text: 'True',
            correct: true
        })
        var answers14 = new models.Answer({
            text: 'incorrect answer',
            correct: false
        })

        var answers21 = new models.Answer({
            text: 'incorrect answer',
            correct: false
        })
        var answers22 = new models.Answer({
            text: 'True',
            correct: true
        })
        var answers23 = new models.Answer({
            text: 'correct answer',
            correct: true
        })
        var answers24 = new models.Answer({
            text: 'incorrect answer',
            correct: false
        })

        var question11 = new models.Question({
            _id: 1,
            text: 'Find the correct answer',
            answers: [answers11, answers12, answers13, answers14]
        })

        var question12 = new models.Question({
            _id: 2,
            text: 'Find the correct answer again',
            answers: [answers21, answers22, answers23, answers24]
        })


        var quizQuestion1 = new models.QuizQuestion({
            quiz: quiz1._id,
            quizName: quiz1.name,
            questions: [question11, question12],
            tl: 300
        })

        quizQuestion1.save(function (err) {
            if (err) throw err;
            logger.info('Quiz Question saved')
        })


        var answers21 = new models.Answer({
            text: 'True',
            correct: true
        })
        var answers22 = new models.Answer({
            text: 'False',
            correct: false
        })

        var answers23 = new models.Answer({
            text: 'False',
            correct: false
        })
        var answers24 = new models.Answer({
            text: 'True',
            correct: true
        })

        var question21 = new models.Question({
            _id: 3,
            text: 'Java is platform independent language.',
            answers: [answers21, answers22]
        })

        var question22 = new models.Question({
            _id: 4,
            text: 'Java main method is entry point to start java program.',
            answers: [answers23, answers24]
        })

        var quizQuestion2 = new models.QuizQuestion({
            quiz: quiz2._id,
            quizName: quiz2.name,
            questions: [question21, question22],
            tl: 300
        })

        quizQuestion2.save(function (err) {
            if (err) throw err;
            logger.info('Quiz Question saved')
        })

*/
//=========================================

        var trainingQuiz1 = new models.TrainingQuiz({
            // _id: 1,
            trainingId: 1,
            quiz:4,
            // quiz:quiz1._id,
            quizName: "AngularJS",
            // quizName: quiz1.quizName,
            // description: quiz1.description
            description: "Training 1: AngularJS quiz"
        })
        var trainingQuiz2 = new models.TrainingQuiz({
            // _id: 2,
            trainingId: 2,
            // quiz:quiz2._id,
            quiz:5,
            // quizName: quiz2.quizName,
            quizName: "ReactJS",
            // description: quiz1.description
            description: "Training2: ReactJS quiz"
        })

        trainingQuiz1.save(function (err) {
            if (err) throw err;
            logger.info('trainingQuiz1 saved');
        })
        trainingQuiz2.save(function (err) {
            if (err) throw err;
            logger.info('trainingQuiz2 saved');
        })

        res.json({
            success: true
        });
    });

}