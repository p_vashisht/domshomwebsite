const jwt = require('jwt-simple');


const config = require('./secret');


function tokenForUser(user) {
  console.log('In tokenFoUser: ' + user);
  const timestamp = new Date().getTime();
  // return jwt.encode({ sub: user.id, iat: timestamp, role }, config.secret);
  return jwt.encode({ sub: user.id, role: user.local.role, iat: timestamp }, config.secret);
}

module.exports = tokenForUser;
