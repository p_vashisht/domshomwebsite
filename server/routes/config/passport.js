// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;


const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const config = require('./secret');

// load up the user model
var User       = require('../../models/user');

// load the auth variables
var configAuth = require('./auth'); // use this one for testing

module.exports = function(passport, logger) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
    // =========================================================================
    // jwt setup ==================================================

    // Setup options for JWT Strategy
    const jwtOptions = {
        // jwtFromRequest: ExtractJwt.fromHeader('Authorization'),
        jwtFromRequest: ExtractJwt.fromHeader('Authorization'),
        secretOrKey: config.secret
    };

    // Create JWT strategy
    const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
        // See if the user ID in the payload exists in our database
        // If it does, call 'done' with that other
        // otherwise, call done without a user object
        logger.debug('Token on sever ****' + payload.sub);
        // logger.debug(jwtOptions.jwtFromRequest)
        User.findById(payload.sub, function(err, user) {
            if (err) { return done(err, false); }
            if (user) {
                done(null, user);
            } else {
                done(null, false);
            }
        });
    });

    passport.use('jwt',jwtLogin);


    // jwt setup ==================================================
    // =========================================================================

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
        if (email)
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

        // asynchronous
        process.nextTick(function() {
            User.findOne({ 'local.email' :  email }, function(err, user) {

                logger.debug("in passport email validation");
                logger.debug(user);
                // logger.debug(req.session.flash);

                // if there are any errors, return the error
                if (err)
                    return done(err);

                // if no user is found, return the message
                if (!user)
                    // return done(null, false, req.flash('loginMessage', 'User not found in database.'));
                    return done(null, false, {'loginMessage': 'User not found in database.'});

                if (!user.validPassword(password))
                    // return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                    return done(null, false, {'loginMessage': 'Oops! Wrong password.'});

                // all is well, return user
                else
                    return done(null, user);
            });
        });

    }));


    

    

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        profileFields   : ['id', 'name', 'email'],
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {
            logger.debug("+++++++++++++++++++++++++++++++")
            logger.debug('process.nextTick');
            logger.debug("+++++++++++++++++++++++++++++++")

            // check if the user is already logged in
            if (!req.user) {

                User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        // if there is a user id already but no token (user was linked at one point and then removed)
                        if (!user.facebook.token) {
                            user.facebook.token = token;
                            user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            user.facebook.email = (profile.emails[0].value || '').toLowerCase();

                            user.save(function(err) {
                                if (err)
                                    return done(err);

                                return done(null, user);
                            });
                        }

                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them
                        var newUser            = new User();

                        newUser.facebook.id    = profile.id;
                        newUser.facebook.token = token;
                        newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                        newUser.facebook.email = (profile.emails[0].value || '').toLowerCase();

                        newUser.save(function(err) {
                            if (err)
                                return done(err);

                            return done(null, newUser);
                        });
                    }
                });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user            = req.user; // pull the user out of the session

                user.facebook.id    = profile.id;
                user.facebook.token = token;
                user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                user.facebook.email = (profile.emails[0].value || '').toLowerCase();

                user.save(function(err) {
                    if (err)
                        return done(err);

                    return done(null, user);
                });

            }
        });

    }));


 

};
