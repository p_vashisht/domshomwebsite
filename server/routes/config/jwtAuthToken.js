var JWT = require('machinepack-jwt');

module.exports = function (authToken, logger, next) {
    JWT.decode({
        secret: require('./secret').secret,
        token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1ODgwZjI1ZGE2MWVkMDFkZDhkNDZiMmEiLCJpYXQiOjE0OTI4NDQ4NjkwNzZ9.kLw6X0E4SkDI1_mXgE3-VdE1bNr8UrZYkMV41FeB99M",
        // token: authToken,
        // algorithm: 'HS256',
        schema: "email,password"
    }).exec({
        error: function (err) {
            // res.status(420).send(err);// invalid token
            res.status(420).redirect('/');// invalid token
        },
        success: function (decodedToken) {
            logger.debug(decodedToken);
            next(decodedToken);
        }
    });
}

/*    app.get('/hello', function (req, res) {
        var JWT = require('machinepack-jwt');
        var newUser = {
            username: 'puneetvashisht@gmail.com',
            password: 'tkhts@123'
        }

        JWT.encode({
            secret: require('./config/secret').secret,
            //  algorithm: 'HS256',
            //  expires: 2880, //in minutes(two days)
            payload: newUser
        }).exec({
            error: function (err) {
                logger.debug(err);
            },
            success: function (authToken) {
                JWT.decode({
                    secret: require('./config/secret').secret,
                        token: authToken,
                     // algorithm: 'HS256',
                        schema: 'username,password'
                     }).exec({
                        error: function (err) {
                            res.send(err);
                        },
                        success: function (decodedToken) {
                            res.send(decodedToken);
                        }
                    });
                }
            });
    });
*/


