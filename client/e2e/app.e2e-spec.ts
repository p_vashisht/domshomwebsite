import { AnotherappPage } from './app.po';

describe('anotherapp App', () => {
  let page: AnotherappPage;

  beforeEach(() => {
    page = new AnotherappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
