import { Component } from '@angular/core';

var myImage1 = require('./../../../public/assets/img/slider/1.jpg');
var myImage2 = require('./../../../public/assets/img/slider/2.jpg');
var myImage3 = require('./../../../public/assets/img/slider/3.jpg');

var aboutUs = require('./../../../public/assets/img/about-us.jpg');

var courses1 = require('./../../../public/assets/img/courses/1.jpg');
var courses2 = require('./../../../public/assets/img/courses/2.jpg');
var courses3 = require('./../../../public/assets/img/courses/3.jpg');

var teacher1 = require('./../../../public/assets/img/teachers/teacher-01.png');
var teacher2 = require('./../../../public/assets/img/teachers/teacher-02.png');
var teacher3 = require('./../../../public/assets/img/teachers/teacher-03.png');
var teacher4 = require('./../../../public/assets/img/teachers/teacher-04.png');

var testimonial1 = require('./../../../public/assets/img/testimonial-1.png');
var testimonial2 = require('./../../../public/assets/img/testimonial-2.png');
var testimonial3 = require('./../../../public/assets/img/testimonial-3.png');

var blog1 = require('./../../../public/assets/img/blog/blog-1.jpg');
var blog2 = require('./../../../public/assets/img/blog/blog-2.jpg');
var blog3 = require('./../../../public/assets/img/blog/blog-3.jpg');

import { CarouselComponent } from 'ngx-bootstrap';

var image1 = require('./images/1.jpg');
var image2 = require('./images/2.jpg');
var image3 = require('./images/3.jpg');
var image4 = require('./images/4.jpg');
var aboutUsImage = require('./images/aboutUsImage.png');
var latestCourseImage = require('./images/750x500.jpg');
var teacherImage = require('./images/teacher.png');
var testimonialImage = require('./images/testimonial.png');
var blogImage = require('./images/blog.jpg');

@Component({
  moduleId: 'module.id',
  styles: [`
	img{
		width: 100%;
	}
  `],
  templateUrl: 'home.component.html'
})
export class HomeComponent {


	private myImage1;
	private myImage2;
	private myImage3;
	
	private courses1;
	private courses2;
	private courses3;
	
	private teacher1;
	private teacher2;
	private teacher3;
	private teacher4;
	
	private testimonial1;
	private testimonial2;
	private testimonial3;
	
	private blog1;
	private blog2;
	private blog3;
	
	private aboutUs;
	
	
	ngOnInit(){
		console.log("myImage1 is : "+myImage1);

		this.myImage1 = myImage1;
		this.myImage2 = myImage2;
		this.myImage3 = myImage3;
		
		this.courses1 = courses1;
		this.courses2 = courses2;
		this.courses3 = courses3;
		
		this.teacher1 = teacher1;
		this.teacher2 = teacher2;
		this.teacher3 = teacher3;
		this.teacher4 = teacher4;
		
		this.testimonial1 = testimonial1;
		this.testimonial2 = testimonial2;
		this.testimonial3 = testimonial3;
		
		this.blog1 = blog1;
		this.blog2 = blog2;
		this.blog3 = blog3;
		
		this.aboutUs = aboutUs;
	} 

	private image1;
	private image2;
	private image3;
	private image4;
	private aboutUsImage;
	private latestCourseImage;
	private teacherImage;
	private testimonialImage;
	private blogImage;

	constructor(){
		this.image1 = image1;
		this.image2 = image2;
		this.image3 = image3;
		this.image4 = image4;
		this.aboutUsImage = aboutUsImage;
		this.latestCourseImage = latestCourseImage;
		this.teacherImage = teacherImage;
		this.testimonialImage = testimonialImage;
		this.blogImage = blogImage;
	}

}