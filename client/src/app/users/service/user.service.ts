import { Injectable } from '@angular/core';
import { User } from "./../../models/user.model";

@Injectable()
export class UserService {
	public user:User = {email: '', token: null};
	public currentPage: string = "/profile"

	constructor(){
		this.user.token = localStorage.getItem('token')
		// if(token){
		// 	this.user.isAuthenticated = true
		// }
	}
}

