import { Injectable } from '@angular/core';
// import { Http, Response, RequestOptions, Headers } from "@angular/http";
import { Http, Response, Headers } from "@angular/http";
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/Rx';
import { Observable } from "rxjs/Rx";

import { User } from "./../../models/user.model";

import { AuthService } from "./../../common/auth.service";

import { Host } from "./../../common/httpHost/host";


@Injectable()
export class UserHttpService {

	// constructor(private http: Http, private authHttp: AuthHttp) { }
	constructor(private host: Host, private http: Http, private authHttp: AuthHttp, private authService: AuthService) { }
 
	login(user: User) {
		console.log('checking token: ...' + localStorage.getItem('token'))
		console.log(user);

		return this.http.post(this.host.hostUrl+'/login', user).map((res: Response) => {
			console.log(res.json());
			return res.json();

		});

	}
	facebooklogin() {
		console.log('facebook service...')

		return this.http.get(this.host.hostUrl+'/auth/facebook').map((res: Response) => {

			console.log("++++++++**************");

			console.log(res.json());
			return res.json();

		});

	}
	signup(user: User) {
		console.log(user);
		return this.http.post(this.host.hostUrl+'/signup', user).map((res: Response) => {
			console.log(res.json());
			return res.json();
		});

	}


	findMakers() {
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('auth', localStorage.getItem('token'));

		return this.http.post(this.host.hostUrl+'/findPendingTrainingMakers', null, {
			headers: headers
		}).map((res: Response) => {
			// console.log(res.json());
			return res.json();
		});
	}

	processPendingMakers(makers, status) {
		console.log('checking token: ...' + localStorage.getItem('token'))
		const data = { "key": "abcdeflkjsdfiujbdsfkhsdfsiudyfsdf54654786bshdfkj%^$^^%$jgsdfshdgf", makers: makers };
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		// headers.append('Authorization', localStorage.getItem('token'));
		headers.append('auth', localStorage.getItem('token'));

		return this.http.post(this.host.hostUrl+'/processPendingTrainingMakers/' + status, data, {
			headers: headers
		}).map((res: Response) => {
			console.log(res.json());
			return res.json();
		});
	}

  sendTrainingRequest(trainingName) {

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('auth', localStorage.getItem('token'));

    return this.authHttp.get(this.host.hostUrl+'/makeUserTraining/'+trainingName, {
      headers: headers
    }).map((res: Response) => {
      return res.json();
    });
  } 



	private handleError(error: any) {
		console.log(error);
		return Observable.throw(error.json());
	}

}
