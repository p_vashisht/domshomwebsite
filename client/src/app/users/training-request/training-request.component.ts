import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { UserHttpService } from "./../service/user.http.service";
// import { AlertModule } from 'ngx-bootstrap';

//import 'jquery/dist/jquery';
//import 'bootstrap/dist/css/bootstrap.css';

@Component({
	// selector: 'ds-training-request',
	styles: [`
	`],
  templateUrl: 'training-request.component.html'
//  template: `<h1>Login page</h1>`
})
export class TrainingRequestComponent {
	@ViewChild('childModalSignIn') public childModalSignIn:ModalDirective;
	constructor(private userHttpService: UserHttpService, private router: Router){
		console.log("request");
	}

	generateTrainingRequest(trainingName: string){
		if(trainingName.trim().length>0){
			this.userHttpService.sendTrainingRequest(trainingName)
				.subscribe(
					data => {
						console.log(data)
					},
					error => {
						console.log(error);
					}
				);
		} else {
			alert("Training name can not be balnk");
		}

	}
}



/*
import { ProgressbarComponent } from 'ngx-bootstrap';
model
import { ProgressbarModule } from 'ngx-bootstrap';
ngMoule
import
	  ProgressbarModule.forRoot(),


*/