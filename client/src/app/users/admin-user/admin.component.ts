import { Component, NgZone } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserHttpService } from "./../service/user.http.service";
import { UserService } from "./../service/user.service";

//import 'jquery/dist/jquery';
//import 'bootstrap/dist/css/bootstrap.css';

@Component({
	selector: 'ds-admin',
	styles: [`
	`],
  templateUrl: 'admin.component.html'
})
export class AdminComponent {

	private errorMessage:string;
	private makers: any[] = [];
	private currentMaker: any;
	private checkerStore: any[] = [];

	makerCheckerForm: FormGroup;

	constructor(private zone: NgZone, private fb: FormBuilder, private userHttpService: UserHttpService, private userService: UserService, private router: Router){
		console.log("admin page");
		this.makerCheckerForm = fb.group({
			// 'name' : [null, Validators.required],
			// 'description' : [null, Validators.compose([Validators.required, Validators.minLength(30), Validators.maxLength(500)])],
			// 'validate' : '',
			'check':''
		});
	}

	ngOnInit(){
		console.log("init");
		// console.log(this.userService);

		// finding the makers
		this.findAllMakerList();

		this.makerCheckerForm.get('check')
			.valueChanges.subscribe((check) => {
				// console.log(check);

				if (check) {
					// console.log('allow: ', this.currentMaker);
					this.checkerStore[this.currentMaker.user] = {"user": this.currentMaker.user, "trainingId": this.currentMaker.trainingId};
				} else {
					// console.log('dont allow: ', this.currentMaker);
					delete this.checkerStore[this.currentMaker.user];
				}
				// console.log(this.checkerStore);
			});

	}

	findAllMakerList(){
			this.userHttpService.findMakers()
				.subscribe(
					data => {
						// console.log(data);
						this.makers = data.userTrainings;
					},
					error => {
						console.log('Server error occurred...')
						console.log(error);
						console.log(error.status);
						this.errorMessage = "Server error occurred!!"
					}
		);
	}

	onSubmit(status){
		// console.log("submit the form now");
		// console.log(this.checkerStore);
		let finalCheckers: any[] = [];
		for(let key in this.checkerStore){
			// console.log(key);
			finalCheckers.push(this.checkerStore[key]);
		}
		// console.log(finalCheckers);

		if(finalCheckers.length > 0){
			this.userHttpService.processPendingMakers(finalCheckers, status)
				.subscribe(
					data => {
						console.log(data);

						// this.makers=null;
						// this.currentMaker = null;
						// this.checkerStore = [];

						// this.findAllMakerList();

						this.router.navigate(['/user/profile']);
						// this.router.navigate(['user','admin'], {skipLocationChange: true});
						// window.location.reload();
/*						this.zone.runOutsideAngular(() => {
							location.reload();
						});*/

					},
					error => {
						console.log('Server error occurred...')
						console.log(error);
						console.log(error.status);
						this.errorMessage = "Server error occurred!!"
					}
		);
		} else {
			alert("You have not selected any maker");
		}
  }

  onChecked(maker){
	//   console.log(maker);
	this.currentMaker = maker;
  }
}
