import { NgModule } from '@angular/core';

import { SignUpComponent } from './signup.component';
import { SignInComponent } from './signin.component';
import { routing } from './user.routing';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UserHttpService } from "./service/user.http.service";
import { UserService } from "./service/user.service";
import { AuthModule } from "./../common/auth.module";

import { AdminComponent } from './admin-user/admin.component';
import { TrainingRequestComponent } from './training-request/training-request.component';

import { AdminGuard } from './../common/guards/admin/admin.guard';
import { AuthGuard } from './../common/auth.guard';

// import { AlertModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    AuthModule,
	  routing,
	  ReactiveFormsModule, 
    // AlertModule.forRoot(),
    ModalModule.forRoot(),
	  FormsModule, 
	  CommonModule// provides inbuild directives
  ],
  exports: [SignInComponent,SignUpComponent],
//  providers: [UserHttpService, UserService],
  providers: [UserHttpService, AdminGuard, AuthGuard],
  declarations: [SignInComponent, SignUpComponent,AdminComponent, TrainingRequestComponent]
})
export class UserModule {}
