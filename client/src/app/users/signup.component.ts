import { Component, Output,EventEmitter } from '@angular/core';
import { NgForm } from "@angular/forms";
import { User } from "../models/user.model";
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';

import { UserHttpService } from "./service/user.http.service";
import { UserService } from "./service/user.service";

//import 'jquery/dist/jquery';
import 'bootstrap/dist/css/bootstrap.css';

@Component({
	selector: 'ds-signup',
	styleUrls: ['signup.component.css'], 
	// styles: [`
	// 	input.ng-valid[required], .ng-valid.required  {
	// 	  border-left: 5px solid #42A948; /* green */
	// 	}
	// 	input.ng-invalid:not(form)  {
	// 	  border-left: 5px solid #a94442; /* red */
	// 	}
	// `],
  templateUrl: 'signup.component.html'
  //template: `<h1>signup page</h1>`
})
export class SignUpComponent {
	user = {email:'', password:'', confirmPassword:''}
	errorMessage:string;
	@Output('onSignUp') onSignUp: EventEmitter<string> = new EventEmitter();

	
	constructor(private userHttpService: UserHttpService, private userService: UserService, private router: Router){
		console.log(this.userService)
	}
	
	onSubmit(form: NgForm) {
		console.log(form.value);
		let user = form.value;
    console.log(user);
	
	this.userHttpService.signup(user)
		.subscribe(
			data => {
				console.log(data)
				this.userService.user = data.user;
				localStorage.setItem('token', data.user.token)
				this.onSignUp.emit('done')
				this.router.navigate(['/profile']);
			},
			error => {
				console.log(error)
				console.log(error.status)
				console.log(error._body.error)
				if(error.status == '422'){
					this.errorMessage = JSON.parse(error._body).error;
				}
			}
		);
	}

}