import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from "@angular/forms";
import { User } from "../models/user.model";
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';

import { UserHttpService } from "./service/user.http.service";
import { UserService } from "./service/user.service";

//import 'jquery/dist/jquery';
//import 'bootstrap/dist/css/bootstrap.css';

@Component({
	selector: 'ds-signin',
	styleUrls:['signin.component.css'],
	// styles: [`
	// 	input.ng-valid[required], .ng-valid.required  {
	// 	  border-left: 5px solid #42A948; /* green */
	// 	}
	// 	input.ng-invalid:not(form)  {
	// 	  border-left: 5px solid #a94442; /* red */
	// 	}
	// `],
  templateUrl: 'signin.component.html'
//  template: `<h1>Login page</h1>`
})
export class SignInComponent {
	// user:User = {email:'', password:'', token: null}
	user:User = {"email":"puneetvashisht@gmail.com", "password":"tkhts@123", "token": null}
	errorMessage:string;
	@Output('onSignIn') onSignIn: EventEmitter<string> = new EventEmitter();

	constructor(private userHttpService: UserHttpService, private userService: UserService, private router: Router){
		console.log(this.userService)
	}

facebookLogin(){
	console.log('logging to facebook');
	this.userHttpService.facebooklogin()
		.subscribe(
			data => {
				console.log(data)
				if(!!data.user.token){
					this.userService.user = data.user;
//					this.router.navigate(['/profile']);
					localStorage.setItem('token', data.user.token)
					this.router.navigate(['/profile/quiz/landing']);
//					this.router.navigate(['/feedback']);
				} else {
					this.errorMessage = data.user.errorMessage;
					this.user.password = '';
				}
			},
			error => {
				console.log(error);
			}
		);

	}


onSubmit(form: NgForm) {
    // console.log(form.value);
	let user = form.value;
    console.log(user);
	
	this.userHttpService.login(user)
		.subscribe(
			data => {
				console.log(data)
				if(!!data.user.token){
					this.userService.user = data.user;
					// console.log(this.userService.user)
					// console.log(this.userService.user.email)
//					this.router.navigate(['/profile']);
					localStorage.setItem('token', data.user.token)
					this.onSignIn.emit('done');
					// this.router.navigate(['/profile/quiz/landing']);
					this.router.navigate([this.userService.currentPage]);
				} else {
					this.errorMessage = data.user.errorMessage;
					this.user.password = '';
				}
			},
			error => {
				console.log('testing error condition')
				console.log(error);
				console.log(error.status);
				this.errorMessage = "Invalid User Name or Password!!"
			}
		);

}
}
