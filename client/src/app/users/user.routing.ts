import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignUpComponent } from './signup.component';
import { SignInComponent } from './signin.component';

import { AdminComponent } from './admin-user/admin.component';

import { AdminGuard } from './../common/guards/admin/admin.guard';
import { TrainingRequestComponent } from './training-request/training-request.component';
import { AuthGuard } from './../common/auth.guard';

const routes: Routes = [
  { path: 'signin', component: SignInComponent },
  { path: 'signup', component: SignUpComponent },
  // { path: 'admin', loadChildren: './quiz/quiz.module#QuizModule', canActivate: [AdminGuard] },
  { path: 'admin', component: AdminComponent, canActivate: [AdminGuard] },
  // { path: 'admin', component: AdminComponent },
  { path: 'newTrainingRequest', component: TrainingRequestComponent, canActivate: [AuthGuard] },


/*{
    path: 'admin',
    component: AdminComponent,
    canActivate: [AdminGuard], 
    children: [
      {path: '', redirectTo: 'a'},
      {path: 'a', component: AComponent},
      {path: 'b', component: BComponent},
    ]
  }
*/
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);