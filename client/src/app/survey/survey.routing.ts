import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ItemComponent } from './item/item.component';
import { NewSurveyComponent } from './new-survey/new-survey.component';
import { ListComponent } from './list/list.component';

import { AdminGuard } from './../common/guards/admin/admin.guard';

const routes: Routes = [
  { path: 'item', component: ItemComponent },
  { path: 'newSurvey', component: NewSurveyComponent, canActivate: [AdminGuard]  },
  { path: 'list', component: ListComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes); 