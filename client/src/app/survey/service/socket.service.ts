import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client/dist/socket.io';

export class SocketService {
  // private url = 'http://192.168.43.144:4000';  
  // private url = 'http://localhost:4000';  
  private url = 'http://192.168.1.130:4000';  
  private socket;

	constructor(){
		this.socket = io.connect(this.url);
		console.log(this.socket);
	}
  
  sendMessage(message){
    this.socket.emit('add-message', message);    
  }
  
  getMessages() {
    let observable = new Observable(observer => {
      // this.socket = io(this.url);
      this.socket.on('message', (data) => {
        observer.next(data);    
      });
      return () => {
        // this.socket.disconnect();
      };  
    })     
    return observable;
  }  

  getNewVote() {
    let observable = new Observable(observer => {
      // this.socket = io(this.url);
      this.socket.on('vote', (data) => {
		  console.log("new vote");
		  // console.log(data);
        observer.next(data);    
      });
      return () => {
        // this.socket.disconnect();
      };  
    })     
    return observable;
  }  

	sendVote(vote){
		console.log("s "+vote);
    	this.socket.emit('send:vote', vote);  
//		alert('vote sent');
	}
}
