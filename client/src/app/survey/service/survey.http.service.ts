import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/Rx';
import { Observable } from "rxjs/Rx";

import { Host } from "./../../common/httpHost/host";

@Injectable()
export class SurveyHttpService {

	constructor(private host: Host, private http: Http, private authHttp: AuthHttp) { }


	item(polls: any) {
		console.log(polls);
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));

		return this.http.post(this.host.hostUrl+'/poll', polls, {
			headers: headers
		}).map((res: Response) => {
			console.log(res.json());
			return res.json();
		});

	}

	newSurvey(survey: any) {
		console.log(survey);
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
	return this.http.post(this.host.hostUrl+'/polls', survey, {
			headers: headers
		}).map((res: Response) => {
			//		console.log(res.json());
			return res.json();
		});
	}

	getSurveyList() {
	const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		return this.http.get(this.host.hostUrl+'/polls/polls', {
			headers: headers
		}).map((res: Response) => {
			//		console.log(res.json());
			return res.json();
		});
	}

	getSurveyItem(surveyId) {
	const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		return this.http.get(this.host.hostUrl+'/polls/' + surveyId, {
			headers: headers
		}).map((res: Response) => {
			//		console.log(res.json());
			return res.json();
		});
	}

	private handleError(error: any) {
		console.log(error);
		return Observable.throw(error.json());
	}

}
