import { Component } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router } from '@angular/router';

import { SurveyHttpService } from "../service/survey.http.service";
import { SurveyService } from "../service/survey.service";
import { SocketService } from "../service/socket.service";

@Component({
	templateUrl: 'item.component.html',
})
export class ItemComponent {

	private poll:any = null;
  	private connection;
	constructor(private surveyHttpService: SurveyHttpService, private surveyService:SurveyService, private socketService:SocketService){
		console.log(this.surveyService);
		this.poll = this.surveyService.survey;
	}
	ngOnInit() {
		this.connection = this.socketService.getNewVote().subscribe(newVote => {
		  console.log(newVote); 
			this.poll = newVote;
		})
	  }
  
  ngOnDestroy() {
    this.connection.unsubscribe();
  }
	onVote() { 
		if(!!this.poll.userVote) {
			let voteObj = { poll_id: this.poll._id, choice: this.poll.userVote };
			console.log(voteObj)
			this.socketService.sendVote(voteObj);
		} else {
			alert('You must select an option to vote for');
		}

	}

}