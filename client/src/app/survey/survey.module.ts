import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ItemComponent } from './item/item.component';
import { NewSurveyComponent } from './new-survey/new-survey.component';
import { ListComponent } from './list/list.component';

import { SurveyHttpService } from "./service/survey.http.service";
import { SurveyService } from "./service/survey.service";
import { SocketService } from "./service/socket.service";
import { routing } from './survey.routing';

import { AuthGuard } from './../common/auth.guard';

@NgModule({
  imports: [routing, FormsModule, CommonModule], 
  providers: [SurveyHttpService, SurveyService, SocketService, AuthGuard],
  declarations: [ ItemComponent, NewSurveyComponent, ListComponent ]
})
export class SurveyModule {}
