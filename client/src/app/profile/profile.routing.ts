import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileComponent } from './profile.component';
// import { TrainingRequestComponent } from './training-request/training-request.component';

import { AuthGuard } from './../common/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },//default path
  { path: 'index', component: ProfileComponent },
  // { path: 'newTrainingRequest', component: TrainingRequestComponent, canActivate: [AuthGuard] },
  { path: 'quiz', loadChildren: './quiz/quiz.module#QuizModule', canActivate: [AuthGuard] },
  { path: 'history', loadChildren: './result-history/result-history.module#ResultHistoryModule', canActivate: [AuthGuard] }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

