import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProgressComponent } from './progress/progress.component';
import { ScoreComponent } from './score/score.component';

const routes: Routes = [
  { path: 'progress', component: ProgressComponent },
  { path: 'score', component: ScoreComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes); 