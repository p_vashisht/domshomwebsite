import { Component } from '@angular/core';

import { QuizHttpService } from './../../service/quiz.http.service';

@Component({
  templateUrl: 'score.component.html'
})
export class ScoreComponent {
	scoreHistory = {}
	constructor(private quizHttpService:QuizHttpService){ }
	
	ngOnInit(){
		this.quizHttpService.loadUserScoreHistory()
			.subscribe(
				data => {
					console.log(data);
					this.scoreHistory = data;
				},
				error => {
					console.log(error)
				}
			);
	}
}
