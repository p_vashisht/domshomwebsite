import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { routing } from './result-history.routing';

import { ProgressComponent } from './progress/progress.component';
import { ScoreComponent } from './score/score.component';


@NgModule({
  imports: [
	  routing,
	  ReactiveFormsModule, 
	  FormsModule, 
	  CommonModule// provides inbuild directives
  ],
//  providers: [QuizHttpService],
  declarations: [ProgressComponent, ScoreComponent]
})
export class ResultHistoryModule {}
