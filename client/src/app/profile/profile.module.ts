import { NgModule } from '@angular/core';

import { ProfileComponent }   from './profile.component';
import { routing } from './profile.routing';

import { QuizHttpService } from './service/quiz.http.service';
import { QuizzesService } from './service/quizzes.service';

import { AuthModule } from "./../common/auth.module";
import { AuthGuard } from "./../common/auth.guard";

// import { TrainingRequestComponent } from './training-request/training-request.component';



@NgModule({
  imports: [AuthModule, routing],
  providers: [QuizHttpService, QuizzesService, AuthGuard],
  declarations: [ProfileComponent]
})
export class ProfileModule {}
