import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssessmentsComponent } from './assessments/assessments.component';
import { LandingComponent } from './landing/landing.component';
import { InfoComponent } from './info/info.component';
import { TestComponent } from './test/test.component';
import { TestSubmittedComponent } from './test.submitted/test.submitted.component';

const routes: Routes = [
  { path: 'assessments', component: AssessmentsComponent },
  { path: 'landing', component: LandingComponent },
  { path: 'info', component: InfoComponent },
  { path: 'test', component: TestComponent },
  { path: 'submitted', component: TestSubmittedComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);