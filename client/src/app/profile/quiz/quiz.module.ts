import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { routing } from './quiz.routing';

import { AssessmentsComponent } from './assessments/assessments.component';
import { LandingComponent } from './landing/landing.component';
import { InfoComponent } from './info/info.component';
import { TestComponent } from './test/test.component';
import { TestSubmittedComponent } from './test.submitted/test.submitted.component';


import { ProgressbarModule } from 'ngx-bootstrap';

import { AuthModule } from "./../../common/auth.module";


@NgModule({
  imports: [
		AuthModule,
	  ProgressbarModule.forRoot(),
	  // Ng2BootstrapModule,
	  routing,
	  ReactiveFormsModule, 
	  FormsModule, 
	  CommonModule// provides inbuild directives
  ],
//  providers: [QuizHttpService, QuizzesService],
  declarations: [LandingComponent, InfoComponent, TestComponent, TestSubmittedComponent, AssessmentsComponent]
})
export class QuizModule {}
