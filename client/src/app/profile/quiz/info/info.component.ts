import { Component } from '@angular/core';

import { QuizzesService } from './../../service/quizzes.service';

@Component({
//  template: `<h1>info.component</h1>`
  templateUrl: 'info.component.html'
})
export class InfoComponent {
	quizDuration = {hr:'',min:'',sec:''}; 

	constructor(private quizzesService:QuizzesService){ 
	}
	
	ngOnInit(){
//		console.log(this.quizzesService);
		this.secondsToHours(this.quizzesService.selectedQuiz.tl);
	}
	
	private secondsToHours(totalSeconds){
		console.log(totalSeconds)
		let hr = Math.floor(totalSeconds / 3600);
		totalSeconds %= 3600;
		
		let min = Math.floor(totalSeconds / 60);
		let sec = totalSeconds % 60;

		if (hr < 10) {
			this.quizDuration.hr = "0" + hr;
		} else {
			this.quizDuration.hr = "" + hr;
		}
		if (min < 10) {
			this.quizDuration.min = "0" + min;
		} else {
			this.quizDuration.min = "" + min;
		}
		if (sec < 10) {
			this.quizDuration.sec = "0" + sec;
		} else {
			this.quizDuration.sec = "" + sec;
		}
	}
}

/*
        var secondsToHours = function (totalSeconds) {
            $scope.remainingTime = {};
            $scope.remainingTime.hr = Math.floor(totalSeconds / 3600);
            totalSeconds %= 3600;
            $scope.remainingTime.min = Math.floor(totalSeconds / 60);
            $scope.remainingTime.sec = totalSeconds % 60;

            if ($scope.remainingTime.hr < 10) {
                $scope.remainingTime.hr = "0" + $scope.remainingTime.hr;
            }
            if ($scope.remainingTime.min < 10) {
                $scope.remainingTime.min = "0" + $scope.remainingTime.min;
            }
            if ($scope.remainingTime.sec < 10) {
                $scope.remainingTime.sec = "0" + $scope.remainingTime.sec;
            }
        };
        secondsToHours($scope.testService.tl);

*/