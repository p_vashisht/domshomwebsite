import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { QuizHttpService } from './../../service/quiz.http.service';
import { QuizzesService } from './../../service/quizzes.service';

import { Questions } from './../../../models/question.model';

import { UserService } from "../../../users/service/user.service";

@Component({
	encapsulation: ViewEncapsulation.Emulated,	
	templateUrl: 'landing.component.html',
	styles:[`
		a {
			cursor: pointer;
		}
		a.disabled {
			pointer-events: none;
			cursor: not-allowed;
		}
	`]
})
export class LandingComponent {
	
	constructor(private quizHttpService: QuizHttpService, private quizzesService: QuizzesService, private router: Router, private userService:UserService){ }
	
	ngOnInit(){
		console.log('get user quizzes');
		console.log(this.userService.user.email);
		this.quizHttpService.loadUserQuizzes(this.userService.user.email)
			.subscribe(
				data => {
					console.log(data);
//					data.map(quiz=>delete quiz.__v);
					// this.quizzesService.quizzes.status = data.status;
					this.quizzesService.quizzes = data;
					console.log(this.quizzesService.quizzes);
				},
				error => {
					console.log(error)
				}
			);
	}
	
	getSelectedQuiz(quiz:any){
			console.log(quiz.trainingQuiz);
		if(quiz.status == "accepted"){
		let quizId = quiz.trainingQuiz.quiz;		
		console.log(quizId);
		
		this.quizHttpService.loadSelectedQuiz(quizId)
			.subscribe(
				data => {
					console.log(data);
					this.quizzesService.selectedQuiz = data.result;
					this.quizzesService.selectedQuiz.showScores = data.showScores;
					this.router.navigate(['/profile/quiz/info']);
				},
				error => {
					console.log(error)
				}
			);
		} else {
			alert("Please contact admin for this quiz...")
		}
	}
}
