import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { ProgressbarComponent } from 'ngx-bootstrap';
//import { isBs3 } from 'ng2-bootstrap';

import { QuizzesService } from './../../service/quizzes.service';
import { QuizHttpService } from "./../../service/quiz.http.service";

import { UserService } from "../../../users/service/user.service";

@Component({
  templateUrl: 'test.component.html'
//  templateUrl: 'test.component2.html'
})
export class TestComponent {
	private setIntervalId:any;
	private isTestRunning:boolean = false;
	private progressBar = {
		showWarning:false,
		dynamic:0,
		type:'success',
		maxTime:0
	}
	
	private runProgressBar(): void {
		let totalTimeForTest = this.quizzesService.selectedQuiz.tl;
		this.secondsToHours(totalTimeForTest-this.progressBar.dynamic);
	
		if (this.progressBar.dynamic < totalTimeForTest/4) {
		  this.progressBar.type = 'success';
		} else if (this.progressBar.dynamic < totalTimeForTest/2) {
		  this.progressBar.type = 'info';
		} else if (this.progressBar.dynamic < totalTimeForTest*3/4) {
		  this.progressBar.type = 'warning';
		} else {
		  this.progressBar.type = 'danger';
		}
		if (this.progressBar.dynamic >= totalTimeForTest) {
			clearInterval(this.setIntervalId);
			this.submitTest();
			console.log("Time's up");
		}
 
		this.progressBar.showWarning = (this.progressBar.type === 'danger' || this.progressBar.type === 'warning');
		this.progressBar.dynamic += 1;
	}
	private quizDuration = {hr:'',min:'',sec:''}; 
	private secondsToHours(remainingSeconds){
//		console.log(remainingSeconds);
		let hr = Math.floor(remainingSeconds / 3600);
		remainingSeconds %= 3600;
		
		let min = Math.floor(remainingSeconds / 60);
		let sec = remainingSeconds % 60;

		if (hr < 10) {
			this.quizDuration.hr = "0" + hr;
		} else {
			this.quizDuration.hr = "" + hr;
		}
		if (min < 10) {
			this.quizDuration.min = "0" + min;
		} else {
			this.quizDuration.min = "" + min;
		}
		if (sec < 10) {
			this.quizDuration.sec = "0" + sec;
		} else {
			this.quizDuration.sec = "" + sec;
		}
	}
	constructor(private quizzesService: QuizzesService, private router:Router, private quizHttpService:QuizHttpService, private userService:UserService){
		console.log(this.quizzesService.selectedQuiz.quizName);
		console.log(this.quizzesService.selectedQuiz.questions);
	}
	
	ngOnInit(){
		this.isTestRunning = true;
		this.progressBar.maxTime = this.quizzesService.selectedQuiz.tl;
		this.setIntervalId = setInterval(() => {
			this.runProgressBar();
		}, 1000);
		
		console.log(this.quizzesService.selectedQuiz);
		console.log(this.quizzesService.selectedQuiz.questions.length);
		
		this.quizzesService.selectedQuiz.currentIndex = 0;
        this.quizzesService.selectedQuiz.currentQuestion = this.quizzesService.selectedQuiz.questions[this.quizzesService.selectedQuiz.currentIndex];

        // COLOR FOR BTN IS IMP
        for (var i = 0; i < this.quizzesService.selectedQuiz.questions.length; i++) {
            this.quizzesService.selectedQuiz.questions[i].btnColor = "btn btn-primary";
            //console.dir(i+" - " +this.quizzesService.selectedQuiz.questions[i].colorArray);
        }
	}
	
	
        private nextQuestion() {
            console.log('Previous index was :' + this.quizzesService.selectedQuiz.currentIndex);
            this.attempt(this.quizzesService.selectedQuiz.currentIndex);
            this.quizzesService.selectedQuiz.currentQuestion = this.quizzesService.selectedQuiz.questions[++this.quizzesService.selectedQuiz.currentIndex];
        }
        private previousQuestion() {
            console.log('Previous index was :' + this.quizzesService.selectedQuiz.currentIndex);
            this.attempt(this.quizzesService.selectedQuiz.currentIndex);
            this.quizzesService.selectedQuiz.currentQuestion = this.quizzesService.selectedQuiz.questions[--this.quizzesService.selectedQuiz.currentIndex];
        }

        private goToQuestion(index) {
            console.log('Previous index was :' + this.quizzesService.selectedQuiz.currentIndex);
            console.log('Index changing to :' + index);
            this.attempt(this.quizzesService.selectedQuiz.currentIndex);
			this.quizzesService.selectedQuiz.currentIndex = index;
            this.quizzesService.selectedQuiz.currentQuestion = this.quizzesService.selectedQuiz.questions[index];
//            this.attempt(this.quizzesService.selectedQuiz.currentIndex);
//            console.dir(this.quizzesService.selectedQuiz.currentQuestion.answers);
        }

		private attempt(currentIndex) {
			console.log(this.quizzesService.selectedQuiz.currentQuestion);
			for(var i=0;i<this.quizzesService.selectedQuiz.currentQuestion.answers.length;i++){
				var answer = this.quizzesService.selectedQuiz.currentQuestion.answers[i];
				if (answer.correct) {
					this.quizzesService.selectedQuiz.questions[currentIndex].btnColor = "btn btn-success";
					break;
				} else {
					this.quizzesService.selectedQuiz.questions[currentIndex].btnColor = "btn btn-warining";
				}
			}
		}
	
	submitTest(){
		console.clear();
		// console.log(this.quizzesService.selectedQuiz);
//		delete this.quizzesService.selectedQuiz.currentIndex;
//		delete this.quizzesService.selectedQuiz.currentQuestion;
		

		if(this.isTestRunning){
			this.quizHttpService.submitQuizTest(this.userService.user.email)
				.subscribe(
					data => {
						console.log(data);
						this.isTestRunning = false;
						this.router.navigate(['/profile/quiz/submitted']);
					},
					error => {
						console.log(error)
					}
				);
		} else {
			console.log("Already submitted...");
		}
	}


}