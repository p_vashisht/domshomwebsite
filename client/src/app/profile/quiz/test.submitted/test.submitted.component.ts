import { Component } from '@angular/core';
import { QuizzesService } from './../../service/quizzes.service';

@Component({
  templateUrl: 'test.submitted.component.html'
})
export class TestSubmittedComponent {
	constructor(private quizzesService:QuizzesService){ }
}
