import { Injectable } from '@angular/core';
import { Quizzes, Quiz } from './../../models/quiz.model';

@Injectable()
export class QuizzesService {
	public quizzes:Quizzes = null;
	public selectedQuiz:Quiz = null;
}