import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/Rx';
import { Observable } from "rxjs/Rx";

import { QuizzesService } from "./quizzes.service";
import { AuthService } from "./../../common/auth.service";

//import { Quiz } from "./../models/quiz.model";

import { Host } from "./../../common/httpHost/host";

@Injectable()
export class QuizHttpService {

  constructor(private host: Host, private http: Http, private authHttp: AuthHttp, private quizzesService: QuizzesService, private authService:AuthService) { }

  loadUserQuizzes(email) {
    this.authService.useJwtHelper();
    
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    // headers.append('Authorization', localStorage.getItem('token'));
    headers.append('auth', localStorage.getItem('token'));

    // const headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
    // const options = new RequestOptions({ headers: headers });
 
    // return this.authHttp.post('/findUserQuiz', { email: email }, {
    return this.authHttp.post(this.host.hostUrl+'/findUserTrainingQuiz', { email: email }, {
      headers: headers
    }).map((res: Response) => {
      		// console.log(res.json());
      return res.json();
    });
  } 

  loadSelectedQuiz(quizId: number) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('auth', localStorage.getItem('token'));
    return this.authHttp.get(this.host.hostUrl+'/findQuizQuestions/' + quizId, {
      headers: headers
    }).map((res: Response) => {
      //		console.log(res.json());
      return res.json();
    });
  }

  //  submitQuizTest(quizToSubmit:Quiz) {
  submitQuizTest(email) {
    console.log(this.quizzesService.selectedQuiz);
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('auth', localStorage.getItem('token'));
    return this.authHttp.post(this.host.hostUrl+'/submitQuiz', { email: email, selectedQuiz: this.quizzesService.selectedQuiz }, {
      headers: headers
    }).map((res: Response) => {
      //		console.log(res.json());
      return res.json();
    });
  }

  loadUserScoreHistory() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('auth', localStorage.getItem('token'));
    return this.authHttp.get(this.host.hostUrl+'/user/scores', {
      // headers: headers
    }).map((res: Response) => {
      //		console.log(res.json());
      return res.json();
    });
  }



  private handleError(error: any) {
    console.log(error);
    return Observable.throw(error.json());
  }

}
