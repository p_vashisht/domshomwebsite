import { NgModule } from '@angular/core';

import { ArchiveComponent } from './archive.component';
import { DetailComponent } from './detail.component';
import { routing } from './course.routing';

@NgModule({
  imports: [routing],
  declarations: [ArchiveComponent, DetailComponent]
})
export class CourseModule {}
