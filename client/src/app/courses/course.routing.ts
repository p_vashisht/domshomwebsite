import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArchiveComponent } from './archive.component';
import { DetailComponent } from './detail.component';

const routes: Routes = [
  { path: 'archive', component: ArchiveComponent },
  { path: 'detail', component: DetailComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);