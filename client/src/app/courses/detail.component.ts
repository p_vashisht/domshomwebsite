import { Component } from '@angular/core';
import 'bootstrap/dist/css/bootstrap.css';

var courses1 = require('./../../../public/assets/img/courses/1.jpg');
var courses2 = require('./../../../public/assets/img/courses/2.jpg');
var courses3 = require('./../../../public/assets/img/courses/3.jpg');

@Component({
templateUrl: 'detail.component.html'
 // template: '<p>Detail Component</p>'
})
export class DetailComponent {

	private courses1;
	private courses2;
	private courses3;
	
	constructor(){ }
	ngOnInit(){
		this.courses1 = courses1;
		this.courses2 = courses2;
		this.courses3 = courses3;	
	}
}