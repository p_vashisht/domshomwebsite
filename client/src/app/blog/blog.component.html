<section id="mu-page-breadcrumb">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="mu-page-breadcrumb-area">
					<h2>Blog Area</h2>
				</div>
			</div>
		</div>
	</div>
</section>


<section id="mu-contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="mu-contact-area">
					<!-- start title -->
					<div class="mu-title">
						<h2>Angular 2 Components: Inputs and Outputs</h2>
						<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores ut laboriosam corporis doloribus, officia, accusamus illo nam tempore totam alias!</p>-->
					</div>
					<!-- end title -->
					<!-- start contact content -->
					<div class="mu-contact-content">
						<div class="row">
							<div class="col-md-12">

								<!--------------------------------------------------------------->
									<div class="l-mb4 l-pa3 t-bg-white m-border">
						
						
						<div class="ArticleCopy language-javascript">
							
							<p><em>This article is part of a web development series from Microsoft. Thank you for supporting the partners who make SitePoint possible.</em></p>
							<p><em>This is the second part in the Angular 2 series. You can read part one <a href="http://www.sitepoint.com/getting-past-hello-world-angular-2/">here</a>.</em></p>
							<p>In this article we’ll take a look a bit closer at components — how they are defined and how to get data into them and back out from them. We covered the basic idea of components and decorators in an earlier article, and have specifically seen the <code class=" language-javascript">@Component</code> and <code class=" language-javascript">@View</code> decorators used to build an Angular application; this article dives in a bit deeper. However, we can’t cover everything about components in a single article so future articles will take up other aspects of Angular components.
								<br>
							</p>
							
							<br> The code for this article and the other articles in the series is available as in the <a href="https://github.com/DevelopIntelligenceBoulder/angular2-samples">angular2-samples</a> repo. You can also see the samples running at: <a href="http://angular2-samples.azurewebsites.net/">http://angular2-samples.azurewebsites.net/</a>.
							<p></p>
							<p>Although it is possible to write <a href="https://angular.io/">Angular 2</a> applications in ECMAScript 5 (the most common version of JavaScript supported by browsers), we prefer to write in <a href="http://www.typescriptlang.org/">TypeScript</a>. Angular 2 itself is written in TypeScript and it helps us at development time and includes features that make it easier for us to define Angular components.</p>
							<p>In particular, TypeScript supports decorators (sometimes referred to as “annotations”) which are used to declaratively add to or change an existing “thing”. For example, class decorators can add metadata to the class’s constructor function or even alter how the class behaves. For more information on decorators and the types of things you can do with them, see the proposal for <a href="https://github.com/wycats/javascript-decorators">JavaScript decorators</a>. Angular 2 includes several decorators. </p>
							<p>As we’ve covered in an earlier article, components are the key building block for Angular applications. They include a view, defined with HTML and CSS, and an associated controller that implements functionality needed by the view. The controller has three major responsibilities:</p>
							<ul>
								<li>Manage the model, i.e. the application data used by the view,</li>
								<li>Implement methods needed by the view for things like submitting data or hiding/showing sections of the UI, and</li>
								<li>Managing data related to the state of the view such as which item in a list is currently selected.</li>
							</ul>
							<p>Depending on your background, the above list might sound familiar. In fact, the Angular component controller sounds very much like the original definition of a view model as defined by John Gossman in 2005: </p>
							<blockquote>
								<p>The term means “Model of a View”, and can be thought of as abstraction of the view, but it also provides a specialization of the Model that the View can use for data-binding. In this latter role the ViewModel contains data-transformers that convert Model types into View types, and it contains Commands the View can use to interact with the Model.</p>
								<p><a href="http://blogs.msdn.com/b/johngossman/archive/2005/10/08/478683.aspx?WT.mc_id=16549-DEV-sitepoint-article85">http://blogs.msdn.com/b/johngossman/archive/2005/10/08/478683.aspx</a> captured 11/27/2015</p>
							</blockquote>
							<p>Because components are not native JavaScript entities, Angular provides a way to define a component by pairing a constructor function with a view. You do this by defining a constructor function (in TypeScript it is defined as a class) and using a decorator to associate your view with the constructor. The decorator can also set various configuration parameters for the component. This magic is accomplished using the <code class=" language-javascript">@Component</code> decorator which we saw in the first article in this series [[include link to Second Angular App article]]. </p>
							<h2>Component Hierarchy</h2>
							<p>The above describes an individual component but Angular 2 applications are actually made up of a hierarchy of components – they begin with a root component that contains as descendants all the components used in the application. Components are intended to be self-contained because we want to encapsulate our component functions and we don’t want other code to arbitrarily reach into our components to read or change properties. Also, we don’t want our component to affect another component written by someone else. An obvious example is CSS – if we set CSS for one component we don’t want our CSS to “bleed out” to another components just as we don’t want other CSS to “bleed in” to our component. </p>
							<p>At the same time components do need to exchange data. In Angular 2, a component can receive data from its parent as long as the receiving component has specifically said it is willing to receive data. Similarly, components can send data to their parents by trigger an event the parent listens for. Let’s look at how the component hierarchy behaves. To begin, we can draw it as follows:</p>
							<p><img [src]="courses1" alt="Component Hierarchy behaviour" width="330" height="240"></p>
							<p>Each box is a component and technically this representation is called “graph” – a data structure consisting of nodes and connecting “edges.” The arrows represent the data flow from one component to another and we can see that data flows in only one direction – from the top downwards to descendants. Also, note there are no paths that allow you to travel from one node, through other nodes and back to the one where you started. The official name for this kind of data structure is that a “directed acyclic graph”, i.e. it flows in only one direction and has no circular paths in it. </p>
							<p>This kind of structure has some important features: it is predictable, it is simple to traverse and it is easy to see what is impacted when a change is made. For Angular’s purposes, when data changes in one node, it is easy to find the downstream nodes that could be affected. </p>
							<p>A simple example of how this might be used is a table with rows containing customers and information about them in which a table component contains multiple individual row components that represent each customer. The table component could manage a record set containing all the customers and pass the data on an individual customer to each of the row components it contains. </p>
							<p>This works fine for simply displaying data but in the real world data will need to flow the other way – back up the hierarchy – such as when a user edits a row. In that case the row needs to tell the table component that the data for a row has changed so the change can be sent back to the server. The problem is that as diagrammed above, data only flows down the hierarchy, not back up. To ensure we maintain the simplicity of one-way data flow down the hierarchy, Angular 2 uses a different mechanism for sending data back up the hierarchy: events. </p>
							<p><img [src]="courses2" alt="One-way data flow down the hierarchy" width="417" height="250" class="aligncenter size-full wp-image-123871"></p>
							<p>Now, when a child component takes an action that a parent needs to know about, the child fires an event that is caught by the parent. The parent can take any action it needs which might include updating data that will, through the usual one-way downwards data flow, update downstream components. By separating the downward flow of data from the upwards flow of data, things are kept simpler and data management performs well.</p>
							<h2>Inputs and Outputs</h2>
							<p>With that high-level look at components under our belt, let’s look at two properties that can be passed to the <code class=" language-javascript">@Component</code> decorator to implement the downward and upward flow of data: “inputs” and “outputs.” These have sometimes been confusing because in earlier version of the Angular 2 alpha, they were called “properties” (for “inputs”) and “events” (for “outputs”) and some developers were less than enthralled with the name change, though it does seem to make sense: <a href="https://github.com/angular/angular/pull/4435">https://github.com/angular/angular/pull/4435</a>. </p>
							<p>“Inputs”, as you might guess from the hierarchy discussion above, specifies which properties you can set on a component whereas “outputs” identifies the events a component can fire to send information up the hierarchy to its parent.</p>
							<p><img [src]="courses3" alt="Using inputs and outputs properties in the @Component decorator" width="747" height="562" class="aligncenter size-full wp-image-123872">
								<br> Figure 1: A component that uses “inputs” and “outputs” properties in the <code class=" language-javascript">@Component</code> decorator</p>
							<p>There are several things to notice with regards to inputs and outputs above:</p>
							<ul>
								<li>The “inputs” property passed to the <code class=" language-javascript">@Components</code> decorator lists “myname” as a component property that can receive data. We also declare “myname” as a public property within <code class=" language-javascript">ParentComp</code> class – if you don’t declare it, the TypeScript compiler might issue a warning.</li>
								<li>The “outputs” property lists “myevent” as a custom event that <code class=" language-javascript">ParentComp</code> can emit which its parent will be able to receive. Within the ParentComp class, “myevent” is declared as and set to be an <code class=" language-javascript">EventEmitter</code>. <code class=" language-javascript">EventEmitter</code> is a built-in class that ships with Angular that gives us methods for managing and firing custom events. Notice that we had to add <code class=" language-javascript">EventEmitter</code> to the import statement at the top of the file.</li>
								<li>This component displays the incoming “myname” in the view but when we try to access it in <code class=" language-javascript">ParentComp</code> constructor it is not yet defined. That is because input properties are not available until the view has rendered which happens after the constructor function runs.</li>
								<li>We added a “click” event handler to our template that invokes the myeventEventEmitter’s “<code class=" language-javascript"><span class="token function">next</span><span class="token punctuation">(</span><span class="token punctuation">)</span></code>” method and passes it the data we want to send with the event. This is the standard pattern for sending data up the component hierarchy – using “EventEmitter” to call the “<code class=" language-javascript"><span class="token function">next</span><span class="token punctuation">(</span><span class="token punctuation">)</span></code>” method.</li>
							</ul>
							<p>Now that we’ve looked at how to define “inputs” and “outputs” in a component, lets see how to use them. The template for the CompDemo component uses the ParentComp component: </p>
							<p><img [src]="courses4" alt="Using the input and output defined by ParentComp" width="774" height="539" class="aligncenter size-full wp-image-123873">
								<br> Figure 2: This component uses the input and output defined by ParentComp</p>
							<p>The syntax is pretty straightforward for using “ParentComp”: </p>
							
							<p>In both cases, the left side of the attribute refers to something in ParentComp (an input property or an output event) and the right side refers to something that is interpreted in the context of CompDemo (an instance property or a method). </p>
							<p>If you try to set a property on ParentComp without specify it as an input property, Angular won’t throw an error but neither will it set the property. The above pattern – passing data in through an “input” property and sending data out through an “output” event – is the primary way to share data between components. We’ll see in a future article that we can also share data between components by defining services that can be injected into components, effectively giving us a way to share data or functions between components. </p>
							<h2>@Input() and @Output()</h2>
							<p>There is an alternative syntax available to define input properties and output events in a component. In the example above we used the “inputs” and “outputs” properties of the object passed to the <code class=" language-javascript">@Component</code> decorator. Angular also lets us use an <code class=" language-javascript">@Input</code> and <code class=" language-javascript">@Output</code> decorator to get the same result: </p>
							<p><img [src]="courses5" alt="Using the @Input and @Output decorator" width="744" height="532" class="aligncenter size-full wp-image-123874">
								<br> Figure 3: Uses the <code class=" language-javascript">@Input</code> and <code class=" language-javascript">@Output</code> decorator</p>
							<p>In the above version of ParentComp, we dispensed with the “inputs” and “outputs” properties of the <code class=" language-javascript">@Component</code> definition object. Instead, we added “Input” and “Output” to the import command on line 2 and used the <code class=" language-javascript">@Input</code> and <code class=" language-javascript">@Output</code> decorator in the ParentComp class to declare “myname” and “myevent.”</p>
							<p>Whether you use inputs/outputs or <code class=" language-javascript">@Input</code>/<code class=" language-javascript">@Output</code> the result is the same so choosing which one to use is largely a stylistic decision. </p>
							<h2>Wrap-Up</h2>
							<p>In this article we’ve looked in more depth at Angular 2 components, how they relate and how you pass data into them and how to get data back out. We’re still only scratching the surface in terms of components – they are arguably the major feature of Angular 2 and are involved in every aspect of designing and building Angular 2 applications. In future articles, we’ll continue to explore Angular 2 components by looking in more detail at Angular services as a way to re-use code and encapsulate key functionality. </p>
							<h2>More Hands-on with Web Development</h2>
							<p>This article is part of the web development series from Microsoft and <a href="http://www.developintelligence.com/">DevelopIntelligence</a> on practical JavaScript learning, open source projects, and interoperability best practices including <a href="http://blogs.windows.com/msedgedev/2015/05/06/a-break-from-the-past-part-2-saying-goodbye-to-activex-vbscript-attachevent/?WT.mc_id=16549-DEV-sitepoint-article85">Microsoft Edge</a> browser and the new <a href="http://blogs.windows.com/msedgedev/2015/02/26/a-break-from-the-past-the-birth-of-microsofts-new-web-rendering-engine/?WT.mc_id=16549-DEV-sitepoint-article85">EdgeHTML rendering engine</a>.</p>
							<p><a href="http://www.developintelligence.com/">DevelopIntelligence</a> offers instructor-led <a href="http://www.developintelligence.com/catalog/javascript-training">JavaScript Training</a>, <a href="http://www.developintelligence.com/catalog/web-development-training/angularjs">AngularJS Training</a> and other <a href="http://www.developintelligence.com/catalog/web-development-training">Web Development Training</a> for technical teams and organizations.</p>
							<p>We encourage you to test across browsers and devices including Microsoft Edge – the default browser for Windows 10 – with free tools on <a href="https://dev.windows.com/en-us/microsoft-edge/?utm_source=SitePoint&amp;utm_medium=article85&amp;utm_campaign=SitePoint">dev.microsoftedge.com</a>:</p>
							
							<p>More in-depth learning from our engineers and evangelists:</p>
							
						</div>
						
						
					</div>
								<!---------------------------------------------------------------------------->
							</div>

						</div>
					</div>
					<!-- end contact content -->
				</div>
			</div>
		</div>
	</div>
</section>