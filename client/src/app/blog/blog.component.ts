import { Component } from '@angular/core';

var blog1 = require('./../../../public/img/1.png');
var blog2 = require('./../../../public/img/2.png');
var blog3 = require('./../../../public/img/3.png');
var blog4 = require('./../../../public/img/4.png');
var blog5 = require('./../../../public/img/5.png');

@Component({
  templateUrl: 'blog.component.html'
})
export class BlogComponent {

	private blog1;
	private blog2;
	private blog3;
	private blog4;
	private blog5;
	
	constructor(){ }
	ngOnInit(){
		this.blog1 = blog1;
		this.blog2 = blog2;
		this.blog3 = blog3;
		this.blog4 = blog4;
		this.blog5 = blog5;
			
	}
}