export interface Answer{
	_id:string;
	correct:boolean;
	text:string;
}

export interface Question{
	_id:number;
	answers: Answer[];
	length:number;
	text:string;
}

export interface Questions{
	questions: Array<Question>;
	length:number;
}
	

	