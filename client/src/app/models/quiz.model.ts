import { Question, Questions } from './question.model';

export interface Quiz{
	__v?:number;
	_id?:string;
	quiz: number;
	quizName:string;
	user?:string;
    tl?: number;
	showScores?:false;
	questions?:Questions;
	currentQuestion?:Question;
	currentIndex?:number;
}
	
export interface Quizzes{
//	quizzes: Quiz[];
	quizzes: Array<Quiz>;
	length:number;
}
	

	