export interface User{
	email: string;
	// isAuthenticated: boolean;
	password?: string;
	contact?: string;
	token?: string
}