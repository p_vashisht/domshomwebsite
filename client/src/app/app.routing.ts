import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
// import { TrainingRequestComponent } from './users/training-request/training-request.component';
// import { AuthGuard } from './common/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },//default path
  { path: 'home', component: HomeComponent },
  // { path: 'newTrainingRequest', component: TrainingRequestComponent, canActivate: [AuthGuard] },
  { path: 'user', loadChildren: './users/user.module#UserModule' },
  { path: 'videos', loadChildren: './videos/videos.module#VideosModule' },
  { path: 'course', loadChildren: './courses/course.module#CourseModule' },
  { path: 'survey', loadChildren: './survey/survey.module#SurveyModule' },
  { path: 'feedback', loadChildren: './feedback/feedback.module#FeedbackModule' },
  { path: 'gallery', loadChildren: './gallery/gallery.module#GalleryModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfileModule' },

  // only for admin
  // { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
 
  //{ path: 'blog', loadChildren: './blog/blog.module#BlogModule' },

 //{ path: 'slides', loadChildren: './slides/slide.module#SlideModule' },

//  { path: '**', component: PageNotFoundComponent }
 { path: '**', component: HomeComponent }//otherwise
	// {path: '*path', component: HomeComponent}
];

// export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
