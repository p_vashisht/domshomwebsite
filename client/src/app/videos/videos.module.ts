import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from "./videos.routing";
import { LibraryComponent } from './library/library.component';

@NgModule({
  imports: [
    CommonModule,
    routing
  ],
  declarations: [LibraryComponent]
})
export class VideosModule { }
