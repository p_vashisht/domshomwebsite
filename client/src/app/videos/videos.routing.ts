import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibraryComponent } from "./library/library.component";

const routes: Routes = [
  { path: '', redirectTo: 'library', pathMatch: 'full' },//default path
  { path: 'library', component: LibraryComponent }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);