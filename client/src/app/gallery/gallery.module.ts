import { NgModule } from '@angular/core';

import { GalleryComponent }   from './gallery.component';
import { routing } from './gallery.routing';

@NgModule({
  imports: [routing],
  declarations: [GalleryComponent]
})
export class GalleryModule {}
