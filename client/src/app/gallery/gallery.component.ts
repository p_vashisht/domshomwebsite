import { Component } from '@angular/core';
import 'bootstrap/dist/css/bootstrap.css';

var small1 = require('./../../../public/img/gallery/small/1.jpg');
var small2 = require('./../../../public/img/gallery/small/2.jpg');
var small3 = require('./../../../public/img/gallery/small/3.jpg');
var small4 = require('./../../../public/img/gallery/small/4.jpg');
var small5 = require('./../../../public/img/gallery/small/5.jpg');
var small6 = require('./../../../public/img/gallery/small/6.jpg');
var small7 = require('./../../../public/img/gallery/small/7.jpg');
var small8 = require('./../../../public/img/gallery/small/8.jpg');
var small9 = require('./../../../public/img/gallery/small/9.jpg');

/*
var big1 = require('./../../../public/img/gallery/big/1.jpg');
var big2 = require('./../../../public/img/gallery/big/2.jpg');
var big3 = require('./../../../public/img/gallery/big/3.jpg');
var big4 = require('./../../../public/img/gallery/big/4.jpg');
var big5 = require('./../../../public/img/gallery/big/5.jpg');
var big6 = require('./../../../public/img/gallery/big/6.jpg');
var big7 = require('./../../../public/img/gallery/big/7.jpg');
var big8 = require('./../../../public/img/gallery/big/8.jpg');
var big9 = require('./../../../public/img/gallery/big/9.jpg');
*/

@Component({
templateUrl: 'gallery.component.html'
 // template: '<p>Gallery Component</p>'
})
export class GalleryComponent {

	private small1;
	private small2;
	private small3;
	private small4;
	private small5;
	private small6;
	private small7;
	private small8;
	private small9;
	
	/*
	private big1;
	private big2;
	private big3;
	private big4;
	private big5;
	private big6;
	private big7;
	private big8;
	private big9;
	*/
	
	constructor(){ }
	ngOnInit(){
		
		this.small1 = small1;
		this.small2 = small2;
		this.small3 = small3;
		this.small4 = small4;
		this.small5 = small5;
		this.small6 = small6;
		this.small7 = small7;
		this.small8 = small8;
		this.small9 = small9;
		
		/*
		this.big1 = big1;
		this.big2 = big2;
		this.big3 = big3;
		this.big4 = big4;
		this.big5 = big5;
		this.big6 = big6;
		this.big7 = big7;
		this.big8 = big8;
		this.big9 = big9;
		*/
	}

}