import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate() {
    console.log("In CanActivate")
    // if (tokenNotExpired()) {
    if (localStorage.getItem('token')) {
      return true;//allow
    } else {
      // this.router.navigate(['/']);
      alert("You need to sign first");
      return false;//dont allow
    }
  }
}