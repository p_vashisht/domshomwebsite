import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { JwtHelper } from "angular2-jwt";

@Injectable()
export class AdminGuard implements CanActivate {
    private jwtHelper: JwtHelper = new JwtHelper();

  constructor(private router: Router) {
    console.log("admin guard active");
  }

    useJwtHelper() {
      console.log("jwt helper");
      var token = localStorage.getItem('token');

      console.log(
          this.jwtHelper.decodeToken(token),
          this.jwtHelper.getTokenExpirationDate(token),
          this.jwtHelper.isTokenExpired(token)
      );
    }

  canActivate() {
    console.log("In CanActivate")
      const token = localStorage.getItem('token');

      let isAdmin = !this.jwtHelper.isTokenExpired(token) && "admin"===this.jwtHelper.decodeToken(token).role;

    if (isAdmin) {
      return true;//allow
    } else {
      // this.router.navigate(['/']);
      alert("Permission Denied");
      return false;//dont allow
    }
  }
}