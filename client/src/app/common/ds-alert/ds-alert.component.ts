import { Component, EventEmitter, Output, Input } from '@angular/core';
// import { NgForm } from "@angular/forms";
// import { Observable } from 'rxjs/Rx';
// import { Router } from '@angular/router';


@Component({
	selector: 'ds-alert',
	styles: [`
	`],
  templateUrl: 'ds-alert.component.html'
//  template: `<h1>Login page</h1>`
})
export class DsAlertComponent {

	@Input() message:string;
	@Output('onAlert') onAlert: EventEmitter<string> = new EventEmitter();

	constructor(){
		console.log("in ds modal");
	}

}
