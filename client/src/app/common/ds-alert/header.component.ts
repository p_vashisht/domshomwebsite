import { Component,ViewChild } from '@angular/core';
// import { UserService } from "../../services/user.service";

import { UserService } from "../../users/service/user.service";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

import { AuthService } from "./../../common/auth.service";

@Component({ 
	selector: 'my-header',
	styles:[`
		h3{
			color: green;
		}
	`],
//	template: `<h3 (click)="log('hello')">I am header of this app</h3>`
	templateUrl: 'header.component.html'
})

export class HeaderComponent{

/*constructor(private userService: UserService){
		console.log(this.userService);
	}*/
	

	@ViewChild('childModalSignIn') public childModalSignIn:ModalDirective;
	@ViewChild('childModalSignUp') public childModalSignUp:ModalDirective;
 
	public showChildModalSignIn():void {
		this.childModalSignIn.show();
	}
	
	public hideChildModalSignIn():void { 
		this.childModalSignIn.hide();
	}
	public showChildModalSignUp():void {
		this.childModalSignUp.show();
	}
	
	public hideChildModalSignUp():void {
		this.childModalSignUp.hide();
	}

	
	constructor(private userService:UserService, private router:Router, private authService: AuthService){}
	
	isUserLoggedIn(route: string){
		if(this.userService.user.token && localStorage.getItem('token')){
			this.router.navigate([route]);
		}
		else{
			this.userService.currentPage = route;
			this.showChildModalSignIn();
		}
	}
	logout(){
		console.log('Logging out')
		this.userService.user.token = null
		localStorage.removeItem('token');
	}
	log(message: string){
		console.log(message);
	}
}