import { tokenNotExpired } from 'angular2-jwt';
import { Injectable } from '@angular/core';

import { JwtHelper } from "angular2-jwt";

@Injectable()
export class AuthService {
    private jwtHelper: JwtHelper = new JwtHelper();

    constructor() { }
    loggedIn() {
        // console.log("In AuthService")
        return tokenNotExpired();
    }

    useJwtHelper() {
        console.log("jwt helper");
        var token = localStorage.getItem('token');

        console.log(
            this.jwtHelper.decodeToken(token),
            this.jwtHelper.getTokenExpirationDate(token),
            this.jwtHelper.isTokenExpired(token)
        );
        console.log("jwt helper");
    }
}