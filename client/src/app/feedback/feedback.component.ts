import { Component } from '@angular/core';
import { NgForm } from "@angular/forms";
import { User } from "../models/user.model";
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';

import { RatingComponent } from 'ngx-bootstrap';

import { FeedbackHttpService } from "./service/feedback.http.service";
import { UserService } from "../users/service/user.service";

@Component({
	templateUrl: 'feedback.component.html'
})
export class FeedbackComponent {

	private userFeedback = {rate:7, email:'', feedback:''}
	private max:number = 10;
	private overStar:number;
	private percent:number;
	
	constructor(private feedbackHttpService: FeedbackHttpService, private userService:UserService, private router:Router){ }

	ngOnInit() {
			this.userFeedback.email = this.userService.user.email;
/*		if(localStorage.getItem('token')){
			console.log('found');
			this.userFeedback.email = this.userService.user.email;
		} else {
			console.log("not found");
			// this.router.navigate(['/user/signin']);
		}*/
	  }
	
	onSubmit(form: NgForm) {
		console.log(this.userFeedback);
	
		this.feedbackHttpService.feedback(this.userFeedback)
			.subscribe(
				data => {
					console.log(data);
					alert("Thankyou for your valueable successuflly.")
					this.router.navigate(['/']);
//					this.router.navigate(['/user/signin']);
				},
				error => {
					console.log(error)
				}
			);
	}

 
	private hoveringOver(value:number):void {
		this.overStar = value;
		this.percent = 100 * (value / this.max);
	};

	private resetStar():void {
//	  	this.overStar = void 0;
		this.percent = 100 * (this.userFeedback.rate / this.max);
	}
}