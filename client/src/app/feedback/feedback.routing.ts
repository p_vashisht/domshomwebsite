import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeedbackComponent } from './feedback.component';

import { AuthGuard } from './../common/auth.guard';

const routes: Routes = [
  { path: '', component: FeedbackComponent, canActivate: [AuthGuard] }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);