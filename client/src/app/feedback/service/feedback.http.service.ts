import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/Rx';
import { Observable } from "rxjs/Rx";

import { Host } from "./../../common/httpHost/host";

@Injectable()
export class FeedbackHttpService {

  constructor(private host: Host, private http: Http, private authHttp: AuthHttp) { }

  a_123_feedback(userFb: any) {
    const headers = new Headers();
    console.log(userFb);
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', localStorage.getItem('token'));

    return this.http.post(this.host.hostUrl+'/feedback', userFb, { headers: headers }).map((res: Response) => {
      console.log(res.json());
      return res.json();
    });

  }
  feedback(userFb: any) {

    return this.authHttp.post(this.host.hostUrl+'/feedback', userFb).map((res: Response) => {
      console.log(res.json());
      return res.json();
    });

  }

  private handleError(error: any) {
    console.log(error);
    return Observable.throw(error.json());
  }

}
