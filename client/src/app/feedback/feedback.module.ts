import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

// import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';
import { RatingModule } from 'ngx-bootstrap';

import { FeedbackComponent }   from './feedback.component';
import { FeedbackHttpService } from "./service/feedback.http.service";
import { routing } from './feedback.routing';

import { AuthModule } from "./../common/auth.module";
import { AuthGuard } from "./../common/auth.guard";

@NgModule({
  imports: [AuthModule, routing, FormsModule, CommonModule, RatingModule.forRoot()],
  providers: [FeedbackHttpService, AuthGuard],
  declarations: [FeedbackComponent]
})
export class FeedbackModule {}
