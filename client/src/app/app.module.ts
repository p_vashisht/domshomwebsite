import { NgModule } from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import { HttpModule } from "@angular/http";

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { routing } from './app.routing';
import { UserService } from './users/service/user.service';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { RatingModule } from 'ngx-bootstrap';
import { ProgressbarModule } from 'ngx-bootstrap';
import { AlertModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';

import {UserModule} from './users/user.module'
// import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';

// import { AuthGuard } from './common/auth.guard';
import { AuthService } from './common/auth.service';
// import { AUTH_PROVIDERS } from 'angular2-jwt';
// import { TrainingRequestComponent } from './users/training-request/training-request.component';

import { DsAlertComponent } from './common/ds-alert/ds-alert.component';
import { Host } from "./common/httpHost/host";


@NgModule({
  imports: [
    BrowserModule,
	  HttpModule,
    routing,
    AlertModule.forRoot(),
    ProgressbarModule.forRoot(),
    RatingModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    UserModule
  ], 
  providers: [
    Host,
    UserService,  
    AuthService
    // AuthGuard 
    // ...AUTH_PROVIDERS
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    // TrainingRequestComponent,
    HeaderComponent,
    FooterComponent,
    DsAlertComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

