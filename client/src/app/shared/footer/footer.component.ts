import { Component } from '@angular/core';

import { UserService } from "../../users/service/user.service";
import { AuthService } from "./../../common/auth.service";

// import 'bootstrap/dist/css/bootstrap.css';

@Component({
    selector: 'app-footer',
	templateUrl: 'footer.component.html'
})
export class FooterComponent {
	constructor(private userService:UserService, private authService: AuthService){}
}